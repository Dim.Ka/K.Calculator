﻿using NUnit.Framework;

namespace K.Calculator.Doman.Tests
{
    [TestFixture]
    internal class CalculatorTest
    {
        [Test]
        public void PlusTest()
        {
            // Arrange
            var calculator = new Domen.Calculator();

            // Act
            var result = calculator.Plus(2, 3);

            // Assert
            Assert.AreEqual(5, result);
        }
    }
}